using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using KonkuApi.Entities;
using KonkuApi.Helpers;

namespace KonkuApi.Services
{
    public interface IStorageUnitService
    {
        IEnumerable<StorageUnit> GetAll();
        IEnumerable<StorageUnit> GetUserStorageUnits(int id);
        StorageUnit GetById(int id);
        StorageUnit Create(StorageUnit storageUnit, string name);
        void Update(StorageUnit storageUnit, string name = null);
        void Delete(int id);
    }

    public class StorageUnitService : IStorageUnitService
    {
        private DataContext _context;

        public StorageUnitService(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<StorageUnit> GetAll()
        {
            return _context.StorageUnits.Include(s => s.Products);
        }

        public IEnumerable<StorageUnit> GetUserStorageUnits(int id)
        {
            return _context.StorageUnits.Where(x => x.UserId == id)
                                        .ToList();
        }

        public StorageUnit GetById(int id)
        {
            return _context.StorageUnits
                            .Include(s => s.Products)
                            .FirstOrDefault(x => x.StorageUnitId == id);
        }

        public StorageUnit Create(StorageUnit storageUnit, string name)
        {
            // validation
            if (string.IsNullOrWhiteSpace(name))
                throw new AppException("Storage unit name is required");

            _context.StorageUnits.Add(storageUnit);
            _context.SaveChanges();

            return storageUnit;
        }

        public void Update(StorageUnit userParam, string name = null)
        {
            var storageUnit = _context.StorageUnits.Find(userParam.StorageUnitId);

            if (storageUnit == null)
                throw new AppException("Storage unit not found");

            //update name if it has changed
            if (!string.IsNullOrWhiteSpace(userParam.Name) && userParam.Name != storageUnit.Name)
            {
                storageUnit.Name = userParam.Name;
            }

            _context.StorageUnits.Update(storageUnit);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var storageUnit = _context.StorageUnits.Find(id);
            if (storageUnit != null)
            {
                _context.StorageUnits.Remove(storageUnit);
                _context.SaveChanges();
            }
        }

    }
}