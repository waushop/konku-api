using System.Collections.Generic;
using System.Linq;
using KonkuApi.Models.Products;
using KonkuApi.Entities;
using KonkuApi.Helpers;

namespace KonkuApi.Services
{
    public interface IProductService
    {
        IEnumerable<Product> GetAll();
        PagedList<Product> GetUserProducts(int id, ProductParameters productParameters);
        Product GetById(int id);
        Product Create(Product product, string name);
        void Update(Product product, string name = null);
        void Delete(int id);
    }

    public class ProductService : IProductService
    {
        private DataContext _context;

        public ProductService(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<Product> GetAll()
        {
            return _context.Products;
        }

        public PagedList<Product> GetUserProducts(int id, ProductParameters productParameters)
        {
            return PagedList<Product>.ToPagedList(_context.Products.Where(x => x.UserId == id),
                    productParameters.PageNumber,
                    productParameters.PageSize);
        }

        public Product GetById(int id)
        {
            return _context.Products.Find(id);
        }

        public Product Create(Product product, string name)
        {
            // validation
            if (string.IsNullOrWhiteSpace(name))
                throw new AppException("Product name is required");

            _context.Products.Add(product);
            _context.SaveChanges();

            return product;
        }

        public void Update(Product userParam, string name = null)
        {
            var product = _context.Products.Find(userParam.ProductId);

            if (product == null)
                throw new AppException("Product not found");

            //update name if it has changed
            if (!string.IsNullOrWhiteSpace(userParam.Name) && userParam.Name != product.Name)
            {
                product.Name = userParam.Name;
            }

            _context.Products.Update(product);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var product = _context.Products.Find(id);
            if (product != null)
            {
                _context.Products.Remove(product);
                _context.SaveChanges();
            }
        }

    }
}