using System.Collections.Generic;

namespace KonkuApi.Entities
{
    public class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string Role { get; set; }
        public ICollection<StorageUnit> StorageUnits { get; set; }
        
    }
}