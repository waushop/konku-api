using System.Collections.Generic;

namespace KonkuApi.Entities
{
    public class StorageUnit
    {
        public int StorageUnitId { get; set; }
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
        public int UserId { get; set; }

    }
}