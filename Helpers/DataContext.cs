using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using KonkuApi.Entities;

namespace KonkuApi.Helpers
{
    public class DataContext : DbContext
    {
        protected readonly IConfiguration Configuration;

        public DataContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to postgresql server database
            options.UseNpgsql(Configuration.GetConnectionString("KonkuApiDatabase"));
        }

        public DbSet<User> Users { get; set; }
        public DbSet<StorageUnit> StorageUnits { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}