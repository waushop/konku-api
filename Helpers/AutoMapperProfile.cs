using AutoMapper;
using KonkuApi.Entities;
using KonkuApi.Models.Users;
using KonkuApi.Models.StorageUnits;
using KonkuApi.Models.Products;

namespace KonkuApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserModel>();
            CreateMap<RegisterModel, User>();
            CreateMap<UpdateUserModel, User>();

            CreateMap<StorageUnit, StorageUnitModel>();
            CreateMap<CreateStorageUnitModel, StorageUnit>();
            CreateMap<UpdateStorageUnitModel, StorageUnit>();

            CreateMap<Product, ProductModel>();
            CreateMap<CreateProductModel, Product>();
            CreateMap<UpdateProductModel, Product>();
        }
    }
}