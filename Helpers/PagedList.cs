using System;
using System.Collections.Generic;
using System.Linq;

namespace KonkuApi.Helpers
{
    public class PagedList<T> : List<T>
    {
        public int PageIndex { get; private set; }
        public int PageCount { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }

        public bool CanPreviousPage => PageIndex > 1;
        public bool CanNextPage => PageIndex < PageCount;

        public PagedList(List<T> items, int count, int pageNumber, int pageSize)
        {
            TotalCount = count;
            PageSize = pageSize;
            PageIndex = pageNumber;
            PageCount = (int)Math.Ceiling(count / (double)pageSize);

            AddRange(items);
        }

        public static PagedList<T> ToPagedList(IQueryable<T> source, int pageNumber, int pageSize)
        {
            var count = source.Count();
            var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            return new PagedList<T>(items, count, pageNumber, pageSize);
        }
    }
}