# Konku API

Konku API is a user web api server built with .NET Core for Konku storage management web application.

## Installation

Clone this repository to your projects directory

```bash
git clone https://waushop@bitbucket.org/waushop/konku-api.git
```

Fill in your PostreSQL database credentials into appsettings.json file

```bash
Server= your database host
Port= port
User id = your database username
Password =  your database password
Database= your database name
```

## For fresh start delete all files from Migrations folder and add new with following command

```bash
dotnet ef migrations add InitialCreate
```

Update database

```bash
dotnet ef database update
```

## Usage

Start the app in the dev environment

```bash
dotnet run
```

Start the app for production

```bash
dotnet build
```
### Notes
- When both UI and API are running in your machine go to http://localhost:8080 and register a new user. All users registered will be normal users. To get admin user you have to manually change role value User to Admin in users database table (You can use pgAdmin for example). Initial admin user seeding is still in development.
- When creating new products you have to fill on Storage Unit field with it's ID that you get when creating Storage Units. Select box to make that proccess easier and faster is in develoment.

## Technologies used
- .NET Core
- Entity Framework Core
- PostreSQL

## License
[MIT](https://choosealicense.com/licenses/mit/)