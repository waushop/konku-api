using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using KonkuApi.Helpers;
using Microsoft.AspNetCore.Authorization;
using KonkuApi.Services;
using KonkuApi.Entities;
using KonkuApi.Models.StorageUnits;

namespace KonkuApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]

    public class StorageUnitsController : ControllerBase
    {
        private IStorageUnitService _storageUnitService;
        private IMapper _mapper;

        public StorageUnitsController(
            IStorageUnitService storageUnitService,
            IMapper mapper)
        {
            _storageUnitService = storageUnitService;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost]

        public IActionResult Create([FromBody] CreateStorageUnitModel model)
        {
            // map model to entity
            var storageUnit = _mapper.Map<StorageUnit>(model);

            try
            {
                // create storage unit
                _storageUnitService.Create(storageUnit, model.Name);
                return Ok(model);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var storageUnits = _storageUnitService.GetAll();
            var model = _mapper.Map<IList<StorageUnitModel>>(storageUnits);

            return Ok(model);
        }

        [HttpGet("userstorageunits")]
        public IActionResult GetUserStorageUnits()
        {
            var currentUserId = Int32.Parse(User.Identity.Name);
            var storageUnits = _storageUnitService.GetUserStorageUnits(currentUserId);
            var model = _mapper.Map<IList<StorageUnitModel>>(storageUnits);
            return Ok(model);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var storageUnit = _storageUnitService.GetById(id);
            var model = _mapper.Map<StorageUnitModel>(storageUnit);
            return Ok(model);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] UpdateStorageUnitModel model)
        {
            // map model to entity and set id
            var storageUnit = _mapper.Map<StorageUnit>(model);
            storageUnit.StorageUnitId = id;

            try
            {
                // update user 
                _storageUnitService.Update(storageUnit, model.Name);
                return Ok(model);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _storageUnitService.Delete(id);
            return Ok();
        }

    }
}