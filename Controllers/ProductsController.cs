using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using AutoMapper;
using KonkuApi.Helpers;
using Microsoft.AspNetCore.Authorization;
using KonkuApi.Services;
using KonkuApi.Entities;
using KonkuApi.Models.Products;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KonkuApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]

    public class ProductsController : ControllerBase
    {
        private IProductService _productService;
        private IMapper _mapper;

        public ProductsController(
            IProductService productService,
            IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Create([FromForm] CreateProductModel model)
        {
            // map model to entity
            var product = _mapper.Map<Product>(model);

            try
            {
                // create storage unit
                model.ImageName = SaveImage(model.ImageFile);
                product.ImageName = model.ImageName;
                _productService.Create(product, model.Name);
                return Ok(model);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var products = _productService.GetAll();
            var model = _mapper.Map<IList<ProductModel>>(products);
            return Ok(model);
        }

        [HttpGet("userproducts")]
        public IActionResult GetUserProducts([FromQuery] ProductParameters productParameters)
        {
            var currentUserId = Int32.Parse(User.Identity.Name);
            var products = _productService.GetUserProducts(currentUserId, productParameters);
            var model = _mapper.Map<IList<ProductModel>>(products);

            var metadata = new
            {
                products.TotalCount,
                products.PageSize,
                products.PageIndex,
                products.PageCount,
                products.CanNextPage,
                products.CanPreviousPage
            };

            Response.Headers.Add("Pagination", JsonConvert.SerializeObject(metadata,
                                                    new JsonSerializerSettings
                                                    {
                                                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                                                    }));
            Response.Headers.Add("Access-Control-Response-Headers", "Pagination");

            return Ok(model);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var product = _productService.GetById(id);
            var model = _mapper.Map<ProductModel>(product);
            return Ok(model);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] UpdateProductModel model)
        {
            // map model to entity and set id
            var product = _mapper.Map<Product>(model);
            product.ProductId = id;

            try
            {
                // update user 
                _productService.Update(product, model.Name);
                return Ok(model);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _productService.Delete(id);
            return Ok();
        }

        [NonAction]
        public string SaveImage(IFormFile imageFile)
        {
            var image = imageFile;
            string imageName = new String(Path.GetFileNameWithoutExtension(image.FileName).Take(10).ToArray()).Replace(' ', '-');
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(image.FileName);
            var imagePath = Path.Combine(Directory.GetCurrentDirectory(), "Images", imageName);
            using (var fileStream = new FileStream(imagePath, FileMode.Create))
            {
                image.CopyTo(fileStream);
            }
            return imageName;
        }

    }
}