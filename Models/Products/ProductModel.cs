using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;

namespace KonkuApi.Models.Products
{
    public class ProductModel
    {
        public int ProductId { get; set; }

        public string Name { get; set; }

        public string SerialNumber { get; set; }

        public string Condition { get; set; }

        public int Quantity { get; set; }

        public string ImageName { get; set; }

        [NotMapped]
        public IFormFile ImageFile { get; set; }

        [NotMapped]
        public string ImageSrc { get; set; }

        public int StorageUnitId { get; set; }

        public int UserId { get; set; }

    }
}