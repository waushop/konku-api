using System.ComponentModel.DataAnnotations;

namespace KonkuApi.Models.Users
{
    public class RegisterModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        public string Role = "User";
    }
}