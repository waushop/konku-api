using System.Collections.Generic;
using KonkuApi.Entities;

namespace KonkuApi.Models.Users
{
  public class UserModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }

        public ICollection<StorageUnit> StorageUnits { get; set; }

    }
}