using System.Collections.Generic;
using KonkuApi.Entities;

namespace KonkuApi.Models.StorageUnits
{
    public class StorageUnitModel
    {
        public int StorageUnitId { get; set; }
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
   
    }
}