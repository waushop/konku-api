using System.ComponentModel.DataAnnotations;

namespace KonkuApi.Models.StorageUnits
{
    public class CreateStorageUnitModel
    {
        [Required]
        public string Name { get; set; }
        public int UserId { get; set; }

    }
}